import { PLATFORM_ID, Inject, Injectable } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

let isBrowser: boolean;

@Injectable()

export class PlatformDetectService {

    public isBrowser = false;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object
    ) {
        this.isBrowser = (isPlatformBrowser(this.platformId));
        isBrowser = (isPlatformBrowser(this.platformId));
    }
}

export function onlyBrowser(target, name, descriptor) {
    const method = descriptor.value;
    descriptor.value = function(...args) {
        if (!isBrowser) {
            return;
        }
        method.apply(this, args);
    };
}
