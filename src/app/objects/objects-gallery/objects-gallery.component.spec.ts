import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectsGalleryComponent } from './objects-gallery.component';

describe('ObjectsGalleryComponent', () => {
  let component: ObjectsGalleryComponent;
  let fixture: ComponentFixture<ObjectsGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectsGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectsGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
