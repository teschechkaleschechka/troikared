import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutHousingEstateComponent } from './about-housing-estate.component';

describe('AboutHousingEstateComponent', () => {
  let component: AboutHousingEstateComponent;
  let fixture: ComponentFixture<AboutHousingEstateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutHousingEstateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutHousingEstateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
