import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectsNewsComponent } from './objects-news.component';

describe('ObjectsNewsComponent', () => {
  let component: ObjectsNewsComponent;
  let fixture: ComponentFixture<ObjectsNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectsNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectsNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
