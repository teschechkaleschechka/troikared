import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ObjectsLocationMarkers, MARKERS } from './objects-location-markers';
declare let ymaps: any;
declare const $: any;

@Component({
  selector: 'app-objects-location',
  templateUrl: './objects-location.component.html',
  styleUrls: ['./objects-location.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ObjectsLocationComponent implements OnInit {

  public markers: ObjectsLocationMarkers[] = MARKERS;
  public markersConfig = [];
  public activeMarker: string;

  constructor() {
    this.activeMarker = 'may';
  }

  ngOnInit() {
      this.initMap();
    }

  public initMap() {
    let objects_map;

    ymaps.ready(() => {
      objects_map = new ymaps.Map('location__map', {
        center: [55.630, 37.926],
        zoom: 10,
        controls: []
      });

      this.markers.forEach((marker: ObjectsLocationMarkers, index) => {
        this.markersConfig[index] = new ymaps.Placemark(marker.coords, {
          iconContent: `<div class="location__map__marker location__map__marker_${marker.name} ${marker.name === this.activeMarker
                          ? 'location__map__marker_active'
                          : ''}">
                          <img src="${marker.url}" class="location__map__marker__bgr">
                        </div>`
        }, {
          iconLayout: 'default#imageWithContent',
          iconImageHref: '/assets/img/objects/map-marker/marker-transparent.svg',
          iconImageSize: marker.name === this.activeMarker
            ? [94, 94]
            : [66, 66],
          iconImageOffset: marker.name === this.activeMarker
            ? [-47, -47]
            : [-33, -33],
        });
        if (marker.name !== this.activeMarker) {
          this.markersConfig[index].events.add('mouseenter', () => {
            $(`.location__map__marker_${marker.name}`).css('opacity', 1);
          });

          this.markersConfig[index].events.add('mouseleave', () => {
            $(`.location__map__marker_${marker.name}`).css('opacity', 0.7);
          });
        }

        objects_map.geoObjects.add(this.markersConfig[index]);
      });
    });
  }
}
