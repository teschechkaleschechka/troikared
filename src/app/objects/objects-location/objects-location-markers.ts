export interface ObjectsLocationMarkers {
    url: string;
    name: string;
    coords: number[];
}

export const MARKERS: ObjectsLocationMarkers[] = [
    {
        url: '/assets/img/objects/map-marker/gorod-may-100x100.png',
        name: 'may',
        coords: [55.511879, 37.759695]
    }, {
        url: '/assets/img/objects/map-marker/vb-100x100.png',
        name: 'vb',
        coords: [55.533572, 37.723910]
    }, {
        url: '/assets/img/objects/map-marker/oblako-100x100.png',
        name: 'oblaka',
        coords: [55.684840, 37.896622]
    }, {
        url: '/assets/img/objects/map-marker/novokrasovo-100x100.png',
        name: 'novokraskovo',
        coords: [55.663224, 37.998771]
    }, {
        url: '/assets/img/objects/map-marker/bitcev-park-100x100.png',
        name: 'bh',
        coords: [55.551443, 37.689791]
    }, {
        url: '/assets/img/objects/map-marker/academ-100x100.png',
        name: 'academ',
        coords: [55.685324, 37.901015]
    }
];
