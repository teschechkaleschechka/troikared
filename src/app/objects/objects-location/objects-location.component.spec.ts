import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectsLocationComponent } from './objects-location.component';

describe('ObjectsLocationComponent', () => {
  let component: ObjectsLocationComponent;
  let fixture: ComponentFixture<ObjectsLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectsLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectsLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
