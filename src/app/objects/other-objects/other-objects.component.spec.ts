import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherObjectsComponent } from './other-objects.component';

describe('OtherObjectsComponent', () => {
  let component: OtherObjectsComponent;
  let fixture: ComponentFixture<OtherObjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherObjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
