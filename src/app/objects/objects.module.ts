import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObjectsComponent } from './objects.component';
import { RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { PlatformDetectService } from '../platform-detect.service';
import { ObjectsHeaderComponent } from './objects-header/objects-header.component';
import { AboutHousingEstateComponent } from './about-housing-estate/about-housing-estate.component';
import { ObjectsGalleryComponent } from './objects-gallery/objects-gallery.component';
import { ObjectsLocationComponent } from './objects-location/objects-location.component';
import { ObjectsNewsComponent } from './objects-news/objects-news.component';
import { OtherObjectsComponent } from './other-objects/other-objects.component';


const OBJECTS__COMPONENT = [
];

@NgModule({
  declarations: [
    ObjectsComponent,
    ObjectsHeaderComponent,
    AboutHousingEstateComponent,
    ObjectsGalleryComponent,
    ObjectsLocationComponent,
    ObjectsNewsComponent,
    OtherObjectsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'object', component: ObjectsComponent, pathMatch: 'full'}
    ])
  ],
  exports: [
    ObjectsHeaderComponent,
    ObjectsComponent
  ],
  providers: [
    PlatformDetectService
  ],
})
export class ObjectsModule {
}
