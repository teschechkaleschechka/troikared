import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules} from '@angular/router';
import { ROUTES } from './app.routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PlatformDetectService } from './platform-detect.service';
import { HeaderModule } from './header/header.module';
import { FooterModule } from './footer/footer.module';
import { ObjectsModule } from './objects/objects.module';


// Application wide providers
const APP_PROVIDERS = [
  PlatformDetectService
];


const APP_MODULES = [
  HeaderModule,
  FooterModule,
  ObjectsModule,
  // END MY MODULES
  BrowserAnimationsModule,
  BrowserModule,
  BrowserModule.withServerTransition({
    appId: 'my-app-id'
  }),
  FormsModule,
  HttpModule,
  BrowserTransferStateModule,
  HttpClientModule,
  RouterModule,
  RouterModule.forRoot(ROUTES)
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ...APP_MODULES
  ],
  providers: [
    ...APP_PROVIDERS
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public platformDetectService: PlatformDetectService) {}
}
