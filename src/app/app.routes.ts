import { Routes } from '@angular/router';

export const ROUTES: Routes = [
    {path: 'object', loadChildren: './objects/objects.module#ObjectsModule' },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
